//
//  UploadImageAPI.swift
//  LiveMPro
//
//  Created by Vengababu Maparthi on 03/01/18.
//  Copyright © 2018 3Embed. All rights reserved.
//

import Foundation
import Alamofire
class ImageUploadAPI:NSObject{
    
    
    
    /// upload image to server
    ///
    /// - Parameters:
    ///   - imageData: the image data to upload
    ///   - parameters: attaching bid to the image
    ///   - onCompletion: return image url and success response
    ///   - onError: return the error if api fails
    func requestWith(imageData: Data?, onCompletion: ((Bool,String) -> Void)? = nil, onError: ((Error?) -> Void)? = nil){
        
        let url = API.BASE_URL + "uploadImageOnServer"
        
        Alamofire.upload(multipartFormData: { multipartFormData in
            
//            for (key, value) in parameters {
//                if let data = (value as AnyObject).data(using: String.Encoding.utf8.rawValue) {
//                    multipartFormData.append(data, withName: key)
//                }
//            }
            multipartFormData.append(imageData!, withName: "photo", fileName: "image.jpg", mimeType: "image/jpeg")
        },
                         to: url,method:HTTPMethod.post,
                         headers:RXNetworkHelper.getAOTHHeader(), encodingCompletion: { encodingResult in
                            switch encodingResult {
                            case .success(let upload, _, _):
                                upload
                                    .validate()
                                    .responseJSON { response in
                                        switch response.result {
                                        case .success(let value):
                                            do {
                                                if let dictonary = try JSONSerialization.jsonObject(with: response.data!, options: []) as? [String:Any]{
                                                    onCompletion!(true,dictonary["data"] as! String)
                                                }
                                            } catch let error as NSError {
                                                print(error)
                                            }
                                            
                                            print("responseObject: \(value)")
                                        case .failure(let responseError):
                                            print("responseError: \(responseError)")
                                            onCompletion!(false,"")
                                        }
                                }
                            case .failure(let encodingError):
                                onCompletion!(false,"")
                                Helper.hidePI()
                                print("encodingError: \(encodingError)")
                            }
        })
    }
}
