//
//  CancelView.swift
//  LiveMPro
//
//  Created by Vengababu Maparthi on 04/10/17.
//  Copyright © 2017 3Embed. All rights reserved.
//

import UIKit

protocol cancelDeleteBooking:class
{
    func cancelReason()
}


class CancelView: UIView {
  
    open weak var delegate: cancelDeleteBooking?
    @IBOutlet weak var cancelTableView: UITableView!
    var isShown:Bool = false
    var cancelModel = CanelModel()
    var bookingID:Int64 = 0
    var cancelReasonsArray = [cancelReasons]()
    var selectedIndex = -1
    var startTime:Int64 = 0
     var entTime:Int64 = 0
    var bookingType = 0
    private static var share: CancelView? = nil
    
    static var instance: CancelView {
        
        if (share == nil) {
            
            share = Bundle(for: self).loadNibNamed("CancelXib",
                                                   owner: nil,
                                                   options: nil)?.first as? CancelView
     
            share?.cancelTableView.register(UINib(nibName: "CancelCellXib", bundle: Bundle.main), forCellReuseIdentifier: "cancelCell")
            
        }
        return share!
    }
    
    func getTheCancelData(){
        cancelModel.getCancellationReasonsAPI(completionHandler: { cancelApiResults in
            switch cancelApiResults{
            case .success(let reasons):
                self.cancelReasonsArray = reasons
                self.cancelTableView.reloadData()
                break
            case .failure(let error):
                print(error)
                break
            }
        })
    }
    
    @IBAction func closePopup(_ sender: Any) {
        self.hide()
    }
    
    
    @IBAction func cancelBookingAction(_ sender: Any) {
        if selectedIndex != -1{
            let params : [String : Any] = [
                "bookingId":bookingID as Any,
                "resonId" :cancelReasonsArray[selectedIndex].reasonID]
            
            cancelModel.cancelBooking(dict: params ,completionHandler: { (succeeded) in
                if succeeded{
                    if self.bookingType == 2{
                        ReminderModel().removeReminder(self.startTime, endDate: self.entTime)
                    }
                     
                    self.delegate?.cancelReason()
                    self.hide()
                }
            })
        }else{
            Helper.alertVC(errMSG: "Please select any reason")
        }
    }
    
    
    /// Show Network
    func show() {
        if isShown == false {
            isShown = true
            let window = UIApplication.shared.keyWindow
            self.frame = (window?.frame)!
            
            window?.addSubview(self)
         
          //  self.alpha = 0.0
            UIView.animate(withDuration: 0.5,
                           animations: {
                            
                            let animation = CABasicAnimation(keyPath: "position.y")
                            animation.fromValue = NSNumber(value: 0.0)
                            animation.toValue = NSNumber(value: 320.0)
                            self.layer.add(animation, forKey: "basic")
                         //   self.alpha = 1.0
            })
        }
    }
    
    /// Hide
    func hide() {

        if isShown == true {
            isShown = false
      
            UIView.animate(withDuration: 0.5,
                           animations: {
                            let animation = CABasicAnimation(keyPath: "-position.y")
                            animation.fromValue = NSNumber(value: 0.0)
                            animation.toValue = NSNumber(value: 320.0)
                            self.layer.add(animation, forKey: "basic")
                            CancelView.share = nil
                           // self.alpha = 0.0
            },
                           completion: { (completion) in
                            self.removeFromSuperview()
            })
        }
    }
}

extension CancelView : UITableViewDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return cancelReasonsArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cancelCell") as? CancelTableCell
        cell?.cancelLabel.text = cancelReasonsArray[indexPath.row].reason
        cell?.selectionStyle = .none
        return cell!
    }
}

extension CancelView : UITableViewDataSource{
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let cell = tableView.cellForRow(at: indexPath) as? CancelTableCell {
            cell.cancelButton.isSelected = true
              cell.cancelLabel.textColor = COLOR.APP_COLOR
            selectedIndex = indexPath.row
        }
    }
    
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {

        if let cell = tableView.cellForRow(at: indexPath) as? CancelTableCell {
             cell.cancelButton.isSelected = false
            cell.cancelLabel.textColor = Helper.colorFromHexString(hexCode: "000000")
              selectedIndex = -1
        }
    }
}












