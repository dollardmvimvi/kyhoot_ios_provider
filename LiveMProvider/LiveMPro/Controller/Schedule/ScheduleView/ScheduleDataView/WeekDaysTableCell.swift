//
//  WeekDaysTableCell.swift
//  LiveMPro
//
//  Created by Vengababu Maparthi on 01/11/17.
//  Copyright © 2017 3Embed. All rights reserved.
//

import UIKit

class WeekDaysTableCell: UITableViewCell {

    @IBOutlet weak var selectButton: UIButton!
    @IBOutlet weak var weekDayLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
