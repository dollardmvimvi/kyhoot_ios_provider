//
//  SplashViewController.swift
//  DayRunnerDriver
//
//  Created by Rahul Sharma on 13/04/17.
//  Copyright © 2017 3Embed. All rights reserved.
//

import UIKit

class SplashViewController: UIViewController {
    var window: UIWindow?
    
    @IBOutlet var splashImage: UIImageView!
    
    var mask: CALayer?
    var imageView: UIImageView?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // adjust the splash image to screen according to the screen height
        if(iPHONE.IS_iPHONE_5) //640x1136
        {
           
            splashImage.image = UIImage(named:"splash_640x1136")
        }else if(iPHONE.IS_iPHONE_4s) //640x960
        {
            splashImage.image = UIImage(named:"splash_640x960")
        }else if(iPHONE.IS_iPHONE_6) //750x1134
        {
            splashImage.image = UIImage(named:"splash_750x334")
        }else if(iPHONE.IS_iPHONE_6_Plus) //1242x2208
        {
            splashImage.image = UIImage(named:"splash_1242x2208")
        }else{
            splashImage.image = UIImage(named:"splash_1125x2436")
        }
    }
    
    override func didReceiveMemoryWarning() {
        
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        let deviceID = UIDevice.current.identifierForVendor!.uuidString
        let defaults = UserDefaults.standard
        defaults.setValue(deviceID, forKey:USER_INFO.DEVICE_ID)  // storing the device id
        defaults.synchronize()
        
        Timer.scheduledTimer(timeInterval: 2.0,
                             target: self,
                             selector: #selector(self.methodToBeCalled),
                             userInfo: nil,
                             repeats: false); 
        
    }
    
    /// checks whether the session token is there r not, if "yes" it moves inside r goes to signin
    @objc func methodToBeCalled(){
//        UIApplication.shared.isStatusBarHidden = false
        if Utility.sessionToken == USER_INFO.SESSION_TOKEN {
            self.performSegue(withIdentifier: "toHelpVC", sender: nil)
            _ = Utility.deleteSavedData()
        }
        else{
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let mainViewController  = storyboard.instantiateViewController(withIdentifier: "HomeTabBarController")
            self.navigationController?.isNavigationBarHidden = true
            self.navigationController?.viewControllers.append(mainViewController)
        }
    }
}


