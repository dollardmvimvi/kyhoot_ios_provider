//
//  APICalls.swift
//  LiveMPro
//
//  Created by Rahul Sharma on 05/09/17.
//  Copyright © 2017 3Embed. All rights reserved.
//

import Foundation
import UIKit
import RxCocoa
import RxSwift

class AcceptRejectModel {
    
    var apiCall = APILibrary()
    let disposebag = DisposeBag()
    
    
    
    /// accepts/Decline booking
    ///
    /// - Parameters:
    ///   - params: booking id, status
    ///   - completionHandler: return true, if api got successfull
    
    func acceptRejectModelBooking(params:[String:Any],
                                  completionHandler:@escaping (Bool) -> ()) {
        
        Helper.showPI(message:loading.load)
        let acceptReject = AcceptRejectAPI()
        
        acceptReject.acceptRejectModelBooking(method: API.METHOD.ACCEPTREJECTRESPONSE, parameters: params)
        acceptReject.AcceptReject_Response
            .subscribe(onNext: {responseModel in
                
                let responseCodes : HTTPSResponseCodes = HTTPSResponseCodes(rawValue: responseModel.httpStatusCode)!
                Helper.hidePI()
                switch responseCodes{
                case .dataNotFound:
                    Helper.alertVC(errMSG: responseModel.data["message"] as! String)
                    break
                case .SuccessResponse:
                    completionHandler(true)
                    break
                    
                default:
                    Helper.alertVC(errMSG: responseModel.data["message"] as! String)
                    completionHandler(false)
                    break
                }
            }, onError: {error in
                
                Helper.hidePI()
            }).disposed(by: disposebag)
    }
}


