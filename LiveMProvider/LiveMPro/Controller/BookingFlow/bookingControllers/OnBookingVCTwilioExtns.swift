//
//  OnBookingVCTwilioExtns.swift
//  LSP
//
//  Created by Rahul Sharma on 05/06/19.
//  Copyright © 2019 3Embed. All rights reserved.
//

import UIKit

extension OnBookingViewController{
    
    func initializeTwilioDevice(token:String) {
        
        device = TCDevice.init(capabilityToken: token, delegate: self)
    }
    
    func hangUpCall() {
        
        if (connections != nil) {
            
            connections.disconnect()
            
            if phoneCallView != nil
            {
                phoneCallView?.endTimer()
                phoneCallView?.removeFromSuperview()
                phoneCallView = nil
            }
            
        }
    }
    
    
    func retrieveTwilioToken() {
        
        var request = URLRequest.init(url: URL.init(string: TWILIO_CALL_TOKEN_URL)!,
                                      cachePolicy: .useProtocolCachePolicy,
                                      timeoutInterval: 100)
        
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        request.setValue("application/json", forHTTPHeaderField: "Accept")
        
        request.httpMethod = "POST"
        
        let params:Any = [:]
        
        do {
            
            let jsonData = try JSONSerialization.data(withJSONObject: params,
                                                      options: .prettyPrinted)
            
            request.httpBody = jsonData
            
            let urlSession = URLSession.init(configuration: URLSessionConfiguration.default,
                                             delegate: nil,
                                             delegateQueue: OperationQueue.main)
            
            let urlSessionDataTask = urlSession.dataTask(with: request,
                                                         completionHandler: { (dataResponse, httpResponse, error) in
                                                            
                                                            if let dataResponse = dataResponse {
                                                                
                                                                do {
                                                                    
                                                                    if let jsonResponse = try JSONSerialization.jsonObject(with: dataResponse,
                                                                                                                           options: []) as? [String:Any] {
                                                                        
                                                                        if let token = jsonResponse["tocken"] as? String {
                                                                            
                                                                            self.initializeTwilioDevice(token: token)
                                                                        }
                                                                        
                                                                    }
                                                                    
                                                                    
                                                                    
                                                                    
                                                                } catch {
                                                                    
                                                                    print(error.localizedDescription)
                                                                }

                                                            }
                                                            
                                                            
            })
            
            urlSessionDataTask.resume()
            
            
            
        } catch {
            
            print(error.localizedDescription)
        }
        
    }
    
}
    
    
    

