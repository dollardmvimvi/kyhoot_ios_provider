//
//  OnBookingDistanceCalVC.swift
//  LiveMPro
//
//  Created by Vengababu Maparthi on 18/01/18.
//  Copyright © 2018 3Embed. All rights reserved.
//

import Foundation

extension MyEventsViewController{
    //MARK: - Distance calculation
    //****** distance calculation for each booking and maintains couch db for storing distance***//
    @objc func updatetheDistanceTravelled(){
        let ud = UserDefaults.standard
        if (ud .value(forKey:"currentLat") != nil) {
            if Utility.onGoingBid.length == 0 {
                self.distanceTime.invalidate()
                return
            }
            
            let onGoingBids:[String] = Utility.onGoingBid.components(separatedBy: ",")
            
            var distDict:[String : Any]  = ["lat": ud .value(forKey:"currentLat") as Any,
                                            "long":ud .value(forKey:"currentLog") as Any,
                                            "dist":0]
          
            for dict  in onGoingBids {
                let bookingData = dict.components(separatedBy: "|")
                if (bookingData[1] == "6") || (bookingData[1] == "8"){
                    // after onetheway(6) and after job starts(8)
                    let distanceData = DistanceCalculation().getTheSavedDistance(distanceDocID:bookingData[0] + "_Dist")
                    if !distanceData.isEmpty{
                        let coordinate₀ = CLLocation(latitude: CLLocationDegrees(ud .value(forKey:"currentLat") as! String)!, longitude: CLLocationDegrees(ud .value(forKey:"currentLog") as! String)!)
                        let lat:Double = Double(distanceData["lat"] as! String)!
                        let log:Double = Double(distanceData["long"] as! String)!
                        
                        let coordinate₁ = CLLocation(latitude: CLLocationDegrees(lat), longitude:  CLLocationDegrees(log))
                        let distanceInMeters = coordinate₁.distance(from: coordinate₀)
                        if distanceInMeters > 40 {
                            var distanceLocal:Double = 0.0
                            distanceLocal = distanceInMeters + Double((distanceData["dist"] as?  NSNumber)!)
                            distDict["dist"] = distanceLocal
                            let distData:[String : Any] = ["distanceDict":distDict]
                            DistanceCalculation().addTheNewDistance(distanceDocID: bookingData[0] + "_Dist", dict: distData)
                        }
                    }else{
                        /// creating new document for each booking
                        let distData:[String : Any] = ["distanceDict":distDict]
                        DistanceCalculation().addTheNewDistance(distanceDocID: bookingData[0] + "_Dist", dict: distData)
                    }
                }
            }
        }
    }
}

