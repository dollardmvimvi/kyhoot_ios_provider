//
//  AddressProfileBookingTableCell.swift
//  LiveMPro
//
//  Created by Vengababu Maparthi on 03/10/17.
//  Copyright © 2017 3Embed. All rights reserved.
//

import UIKit

class AddressProfileBookingTableCell: UITableViewCell {

    @IBOutlet weak var bookingID: UILabel!
    @IBOutlet weak var categoryName: UILabel!
    @IBOutlet weak var custName: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
