//
//  CustProfileBookingTableCell.swift
//  LiveMPro
//
//  Created by Vengababu Maparthi on 03/10/17.
//  Copyright © 2017 3Embed. All rights reserved.
//

import UIKit
import FloatRatingView

class CustProfileBookingTableCell: UITableViewCell {

    @IBOutlet weak var categoryName: UILabel!
    @IBOutlet weak var downView: UIView!
    @IBOutlet weak var custRating: FloatRatingView!
    @IBOutlet weak var custName: UILabel!
    @IBOutlet weak var custImage: UIImageView!
    @IBOutlet weak var bookingStatusMsg: UILabel!
    @IBOutlet weak var custAddress: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }

}
