//
//  PaymentMethodTableCell.swift
//  LSP
//
//  Created by Vengababu Maparthi on 10/04/18.
//  Copyright © 2018 3Embed. All rights reserved.
//

import UIKit
//paymentType
class PaymentMethodTableCell: UITableViewCell {

    @IBOutlet weak var paymentType: UILabel!
    @IBOutlet weak var paymentTypeImage: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
