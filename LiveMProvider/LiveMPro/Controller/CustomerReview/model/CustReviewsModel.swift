//
//  CustReviewsModel.swift
//  LiveMPro
//
//  Created by Vengababu Maparthi on 05/12/17.
//  Copyright © 2017 3Embed. All rights reserved.
//

import RxCocoa
import RxSwift
import Foundation

class CustReviewData: NSObject {
    
    var reviewText = ""
    var rating = 0.0
    var reviewBy = ""
    var profilePic = ""
    var timeStamp = 0
}

class CustReviewModel: NSObject {
    
    var averageRating = 0.0
    
    var aboutMe = ""
    
    var custImage = ""
    
    var custName = ""
    
    var reviewInnerData = [CustReviewData]()
    
    let disposebag = DisposeBag()
    

    /// get the customer reviews while accepting
    ///
    /// - Parameters:
    ///   - custData: customer id
    ///   - completionHandler: returns the customer model data
    func getTheReviewByIndexWise(custData:String,completionHandler:@escaping(Bool,CustReviewModel)->()) {
        let custReviewAPI = CustReviewAPI()
        Helper.showPI(message: loading.gettingReviews)
        custReviewAPI.geTheReviewCustomer (method:API.METHOD.CUSTREVIEWS + custData)
        custReviewAPI.CustReview_Response
            .subscribe(onNext: {responseModel in
                let responseCodes : HTTPSResponseCodes = HTTPSResponseCodes(rawValue: responseModel.httpStatusCode)!
                Helper.hidePI()
                
                switch responseCodes{
                case .SuccessResponse:
                    if let reviewData = responseModel.data["data"] as? [String:Any]{
                        completionHandler(true,self.parseTheReviewData(dict:reviewData))
                    }
                    break
                    
                default:
                    Helper.alertVC(errMSG: responseModel.data["message"] as! String)
                    break
                    
                }
            }, onError: {error in
                
                Helper.hidePI()
            }).disposed(by: disposebag)
    }
    
    func parseTheReviewData(dict:[String:Any]) -> CustReviewModel {
        let reviewData = CustReviewModel()
        
        if let avgRating = dict["averageRating"] as? NSNumber {
            reviewData.averageRating = Double(Float(avgRating))
        }
        
        if let about = dict["about"] as? String {
            reviewData.aboutMe = about
        }
        
        if let reviewDictArray = dict["reviews"] as? [[String:Any]]{
            for reviewDict in reviewDictArray{
                let reviews = CustReviewData()
                if let revText = reviewDict["review"] as? String{
                    reviews.reviewText = revText
                }
                if let rating = reviewDict["rating"] as? NSNumber{
                    reviews.rating = Double(Float(rating))
                }
                if let revBy = reviewDict["reviewBy"] as? String{
                    reviews.reviewBy = revBy
                }
                if let proPic = reviewDict["profilePic"] as? String{
                    reviews.profilePic = proPic
                }
                if let rewTime = reviewDict["reviewAt"] as? Int{
                    reviews.timeStamp = rewTime
                }
                reviewInnerData.append(reviews)
            }
        }
        reviewData.reviewInnerData = reviewInnerData
        return reviewData
    }
}
