//
//  SignInViewController.swift
//  DayRunnerDriver
//
//  Created by Rahul Sharma on 13/04/17.
//  Copyright © 2017 3Embed. All rights reserved.
//

import UIKit
import RxCocoa
import RxSwift

class SignInViewController: UIViewController {
    
    var activeTextField = UITextField()
    
    @IBOutlet weak var signupButton: UIButton!
    @IBOutlet var mainScrollView: UIScrollView!
    @IBOutlet var passwrodTextField: FloatLabelTextField!
    @IBOutlet var emailTextField: FloatLabelTextField!
    @IBOutlet weak var view1: UIView!
        @IBOutlet weak var bottomView: UIView!
    @IBOutlet weak var view2: UIView!
    var signinModel = Signin()
    var locationtracker:LocationTracker = LocationTracker.sharedInstance() as! LocationTracker
    @IBOutlet weak var rememberMeButton: UIButton!
    var sessionTkn = String()
    
    var disposeBag = DisposeBag()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        mainScrollView.bringSubview(toFront: signupButton)
        Helper.shadowView(sender: bottomView, width:UIScreen.main.bounds.size.width, height:bottomView.frame.size.height)
        locationtracker.startLocationTracking()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = false
        //Binding textfield and viewmodel data
        passwrodTextField.rx.text
            .orEmpty
            .bind(to: signinModel.password)
            .disposed(by: disposeBag)
        emailTextField.rx.text
            .orEmpty
            .bind(to: signinModel.emailAddress)
            .disposed(by: disposeBag)
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        self.assignSavedData()
    }
    
    
    override func viewWillDisappear(_ animated: Bool) {
    
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @objc func keyboardWillShow(notification: NSNotification) {
    
    }
    
    @objc func keyboardWillHide(notification: NSNotification) {

    }

    //*****hide keyboard*****//
    func dismisskeyBord() {
        view.endEditing(true)
    }
    

    
    func shake(textField:UITextField) {
        textField.shake(10, withDelta: 7, speed: 0.06, shakeDirection: .horizontal)
    }
    
    @IBAction func backAction(_ sender: UIButton) {
        self.navigationController?.isNavigationBarHidden = true
        self.dismiss(animated: true, completion: nil)
    }
}

