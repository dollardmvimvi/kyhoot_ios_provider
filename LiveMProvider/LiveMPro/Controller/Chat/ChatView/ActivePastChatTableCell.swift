//
//  ActivePastChatTableCell.swift
//  LSP
//
//  Created by Vengababu Maparthi on 28/03/18.
//  Copyright © 2018 3Embed. All rights reserved.
//

import UIKit
import Kingfisher

class ActivePastChatTableCell: UITableViewCell {
    @IBOutlet weak var custImage: UIImageView!
    @IBOutlet weak var time: UILabel!
    @IBOutlet weak var date: UILabel!
    @IBOutlet weak var categoryName: UILabel!
    @IBOutlet weak var customerName: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
    func updateThechatDataOnView(data:ChatData) {
        let imageURL = data.custImage
        self.custImage.kf.setImage(with: URL(string: imageURL),
                                   placeholder:UIImage.init(named: "signup_profile_default_image"),
                                   options: [.transition(ImageTransition.fade(1))],
                                   progressBlock: { receivedSize, totalSize in
        },
                                   completionHandler: { image, error, cacheType, imageURL in
        })
        
        self.customerName.text = data.custName
        self.categoryName.text = data.categoryName
        let dataArr = Helper.getTheDateFromTimeStamp(timeStamp: data.date).components(separatedBy: "|")
        date.text =  dataArr[0]
        time.text =  dataArr[1]
    }
}
