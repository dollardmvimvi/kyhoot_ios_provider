//
//  ViewMoreDescription.swift
//  LSP
//
//  Created by Vengababu Maparthi on 09/02/18.
//  Copyright © 2018 3Embed. All rights reserved.
//

import UIKit

class ViewMoreDescription: UIView {
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var contentView: UIView!
        var isShown:Bool = false
    @IBOutlet weak var heightOfServiceName: NSLayoutConstraint!
    
    @IBOutlet weak var servicePrice: UILabel!
    @IBOutlet weak var catName: UILabel!
    @IBOutlet weak var heightOfContentView: NSLayoutConstraint!
    private static var share: ViewMoreDescription? = nil
    
    static var instance: ViewMoreDescription {
        if (share == nil) {
            share = Bundle(for: self).loadNibNamed("ViewDescription",
                                                   owner: nil,
                                                   options: nil)?.first as? ViewMoreDescription
        }
        return share!
    }
    
    /// Show Network
    func show(serviceData:Services?) {
        self.servicePrice.text = Helper.getTheAmtTextWithSymbol(amt:String(describing:serviceData!.serviceFee))
        self.catName.text = serviceData?.serviceName
        self.descriptionLabel.text = serviceData?.serviceDescription
        self.heightOfContentView.constant = self.heightForView(text: self.descriptionLabel.text!, width: UIScreen.main.bounds.size.width - 130)
        
        self.heightOfServiceName.constant = self.heightForView(text: self.catName.text!, width: UIScreen.main.bounds.size.width - 140)
        
        Helper.shadowView(sender: self.contentView, width: UIScreen.main.bounds.size.width - 80, height: self.heightOfContentView.constant)
        
        self.contentView.layer.cornerRadius = 12
        self.contentView.layer.masksToBounds = true
        if isShown == false {
            isShown = true
            let window = UIApplication.shared.keyWindow
            self.frame = (window?.frame)!
            window?.addSubview(self)
            
            self.alpha = 0.0
            UIView.animate(withDuration: 0.5,
                           animations: {
                            self.alpha = 1.0
            })
        }
    }
    
    @IBAction func hideDescriptionView(_ sender: Any) {
         hide()
    }
    /// Hide
    func hide() {
        
        if isShown == true {
            isShown = false
            
            UIView.animate(withDuration: 0.5,
                           animations: {
                            ViewMoreDescription.share = nil
                            self.alpha = 0.0
            },
                           completion: { (completion) in
                            self.removeFromSuperview()
            })
        }
    }
    
    ///This function calculates the height of content Label  ListingCell row's and returns the height of the label.
    func heightForView(text: String, width: CGFloat) -> CGFloat {
        
        let label: UILabel = UILabel(frame: CGRect(x: 0,
                                                   y: 0,
                                                   width: width,
                                                   height: CGFloat.greatestFiniteMagnitude))
        label.numberOfLines = 0
        label.lineBreakMode = NSLineBreakMode.byWordWrapping
        label.text = text
        label.sizeToFit()
        
        return label.frame.height
    }
}

