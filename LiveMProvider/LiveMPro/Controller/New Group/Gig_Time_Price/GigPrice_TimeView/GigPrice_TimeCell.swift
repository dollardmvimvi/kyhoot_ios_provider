//
//  GigPrice_TimeCell.swift
//  LiveMPro
//
//  Created by Rahul Sharma on 20/09/17.
//  Copyright © 2017 3Embed. All rights reserved.
//

import UIKit

class GigPrice_TimeCell: UITableViewCell {
    @IBOutlet weak var amountTf: UITextField!
    
    @IBOutlet weak var timeAndUnits: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
