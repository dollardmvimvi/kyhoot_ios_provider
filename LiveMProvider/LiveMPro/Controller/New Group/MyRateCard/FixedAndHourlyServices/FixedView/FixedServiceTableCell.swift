//
//  FixedServiceTableCell.swift
//  LSP
//
//  Created by Vengababu Maparthi on 02/02/18.
//  Copyright © 2018 3Embed. All rights reserved.
//

import UIKit

class FixedServiceTableCell: UITableViewCell {
    @IBOutlet weak var editServicePrice: UIButton!
    
    @IBOutlet weak var viewMoreButton: UIButton!
    @IBOutlet weak var CheckBoxButton: UIButton!
    @IBOutlet weak var serviceDescription: UILabel!
    @IBOutlet weak var servicePrice: UILabel!
    @IBOutlet weak var serviceName: UILabel!

    @IBOutlet weak var serviceCompletionTime: UILabel!
    @IBOutlet weak var trailing25_47: NSLayoutConstraint!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
