//
//  BankHeaderTableViewCell.swift
//  DayRunnerDriverDev
//
//  Created by Vengababu Maparthi on 18/08/17.
//  Copyright © 2017 3Embed. All rights reserved.
//

import UIKit

class BankHeaderTableViewCell: UITableViewCell {

    @IBOutlet weak var bankHeaderLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
