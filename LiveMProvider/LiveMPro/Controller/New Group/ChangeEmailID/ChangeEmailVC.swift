//
//  ChangeEmailVC.swift
//  LiveMPro
//
//  Created by Vengababu Maparthi on 10/01/18.
//  Copyright © 2018 3Embed. All rights reserved.
//

import UIKit


protocol EditEmailIdDelegate {
    func updatedNewEmailID(emailID: String)
}

class ChangeEmailVC: UIViewController {
        var delegate: EditEmailIdDelegate?
    @IBOutlet weak var emailTF: FloatLabelTextField!
    @IBOutlet weak var borderView: UIView!
        var updateEventsModel = ProfileListModel()

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func changeEmailID(_ sender: Any) {
        if !(emailTF.text?.isValidEmail)!{
            emailTF.becomeFirstResponder()
            Helper.alertVC(errMSG: signup.InValidEmail)
        }
        else if (emailTF.text?.isEmpty)!{
            Helper.alertVC(errMSG: "please enter the email id")
            
        }else{
            updateEmailID()
        }
    }
    
    @IBAction func backToVc(_ sender: Any) {
        self.dismiss(animated: true, completion:  nil)
    }
    
    func updateEmailID(){
        Helper.showPI(message:loading.update)
        let param : [String : Any] =  ["userType":"2",
                                       "email": emailTF.text!]
        
        updateEventsModel.updateTheNewEmailID( params: param,completionHandler: { (succeded) in
            if succeded{
                UserDefaults.standard.set(self.emailTF.text!, forKey: USER_INFO.SAVEDID)

                self.delegate?.updatedNewEmailID(emailID: self.emailTF.text!)
                self.dismiss(animated: true, completion: nil)
            }else{
                
            }
        })
    }
    
    func dismissView(){
        self.view.endEditing(true)
    }
}


// MARK: - Textfield delegate method
extension ChangeEmailVC : UITextFieldDelegate{
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if textField == emailTF{
            borderView.backgroundColor = COLOR.APP_COLOR
        }
        return true
    }
    
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if (textField.text?.isEmpty)!{
            borderView.backgroundColor = Helper.UIColorFromRGB(rgbValue: 0xe1e1e1)
        }else{
            borderView.backgroundColor = COLOR.APP_COLOR
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {   //delegate method
        if !(emailTF.text?.isValidEmail)!{
            emailTF.becomeFirstResponder()
            Helper.alertVC(errMSG: signup.InValidEmail)
        }
        
        dismissView()
        
        return true
    }
}




