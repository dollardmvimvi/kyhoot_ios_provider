//
//  PageContainerVC.swift
//  LSP
//
//  Created by Vengababu Maparthi on 31/01/18.
//  Copyright © 2018 3Embed. All rights reserved.
//

import UIKit
import Kingfisher

class PageContainerVC: UIViewController {
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var serviceImages: UIImageView!
    var pageContentDelegate:LiveMHelpContentDelegate? = nil
    var pageIndex: Int = 0
    var images = [String]()
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if images.count == 0{
            return
        }
        activityIndicator.startAnimating()
        self.serviceImages.kf.setImage(with: URL(string:  (images[pageIndex])),
                                       placeholder:UIImage.init(named: "history_gallery_icon"),
                                       options: [.transition(ImageTransition.fade(1))],
                                       progressBlock: { receivedSize, totalSize in
        },
                                       completionHandler: { image, error, cacheType, imageURL in
                                        self.activityIndicator.stopAnimating()
        })
        // Do any additional setup after loading the view.
    }
    
 
    override func viewWillAppear(_ animated: Bool) {
        pageContentDelegate?.didSelect1(index: pageIndex)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
