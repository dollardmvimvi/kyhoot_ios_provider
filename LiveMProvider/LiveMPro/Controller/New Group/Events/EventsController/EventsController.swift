//
//  EventsController.swift
//  LiveMPro
//
//  Created by Rahul Sharma on 21/09/17.
//  Copyright © 2017 3Embed. All rights reserved.
//

import UIKit


protocol MyeventsDelegate {
    func updateTheEvents(servicesList:[EventModel], serviceNames:String)
}

class EventsController: UITableViewController {
    
    var delegate: MyeventsDelegate?
    @IBOutlet var eventsTableView: UITableView!
    
    var eventData = [EventModel]()
    var updatedEventData = [EventModel]()
    var updateEventsModel = ProfileListModel()
  
    override func viewDidLoad() {
        super.viewDidLoad()

        self.eventsTableView.reloadData()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func saveEvents(_ sender: Any) {
        var statusData = ""
        var statusNames = ""
        updatedEventData = [EventModel]()
        for val in 0 ... eventData.count - 1{
            let indexPath = IndexPath(row: val, section: 0)
            let cell: EventsCell = eventsTableView.cellForRow(at: indexPath) as! EventsCell
            
            if cell.activeSwitch.isOn {
                if statusData.isEmpty{
                    statusData = eventData[indexPath.row].eventID
                    statusNames = eventData[indexPath.row].eventName
                }else{
                    statusData = statusData + "," + eventData[indexPath.row].eventID
                    statusNames = statusNames + "," + eventData[indexPath.row].eventName
                    
                }
                let event = EventModel()
                event.eventID = eventData[indexPath.row].eventID
                event.eventName = eventData[indexPath.row].eventName
                event.eventStatus = 1
                updatedEventData.append(event)
            }else{
                let event = EventModel()
                event.eventID = eventData[indexPath.row].eventID
                event.eventName = eventData[indexPath.row].eventName
                event.eventStatus = 0
                updatedEventData.append(event)
            }
        }
        
        let params : [String : Any] =  [
            "events" :statusData]
        updateEventsModel.profileDataUpdateService( params: params,completionHandler: { (response) in
            if response {
                
                self.delegate?.updateTheEvents(servicesList: self.updatedEventData,serviceNames:statusNames)
                self.dismiss(animated: true, completion: nil)
            }else{
                
            }
        })
    }
    
    
    
    @IBAction func backAction(_ sender: UIButton) {
        
        dismiss(animated: true, completion: nil)
    }
    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return eventData.count
    }
    
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "eventCell", for: indexPath) as! EventsCell
        
        cell.event.text = eventData[indexPath.row].eventName
        cell.selectionStyle = .none
        if eventData[indexPath.row].eventStatus == 0 {
            cell.activeSwitch .setOn(false, animated: true)
        }else{
            cell.activeSwitch .setOn(true, animated: true)
        }
        cell.selectionStyle = .none
        return cell
    }

    
    override func tableView(_ tableView: UITableView,
                            heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 62
    }
    
    override  func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    override func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        
    }
    
}
