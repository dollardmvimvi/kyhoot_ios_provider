//
//  LiveChatViewController.swift
//  CargoDriver
//
//  Created by Vengababu Maparthi on 28/07/17.
//  Copyright © 2017 3Embed. All rights reserved.
//

import UIKit
import WebKit

class LiveChatViewController: UIViewController,WKNavigationDelegate {
    
    var activityIndicator = UIActivityIndicatorView()
    var liveChatModel = LiveChatModel()
    
    var chatView: WKWebView!

    override func loadView() {
        chatView = WKWebView()
        chatView.navigationDelegate = self
        view = chatView
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        updateTheView()
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func updateTheView() {
        activityIndicator = UIActivityIndicatorView(activityIndicatorStyle: .gray)
        activityIndicator.frame = CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: self.view.frame.size.height)
        self.chatView.addSubview(activityIndicator)
        activityIndicator.startAnimating()
        self.requestUrls()
    }
    
    @IBAction func backToVc(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    
    // requesting the live chat url
    func requestUrls(){
        liveChatModel.getTheLiveChatURL { (success,chatUrl) in
            if success{
                self.activityIndicator.stopAnimating()
                let url = URL(string: chatUrl)
                let request = URLRequest(url: url!)
//                self.chatView.loadRequest(request)
                self.chatView.load(request)
                self.chatView.allowsBackForwardNavigationGestures = true
            }else{
                self.activityIndicator.stopAnimating()
            }
        }
    }
    
    //

}

//extension LiveChatViewController : UIWebViewDelegate {
//
//    func webViewDidStartLoad(_ webView: UIWebView) {
//
//    }
//
//    func webViewDidFinishLoad(_ webView: UIWebView) {
//       activityIndicator.stopAnimating()
//    }
//
//    func webView(_ webView: UIWebView, didFailLoadWithError error: Error) {
//         activityIndicator.stopAnimating()
//    }
//}
