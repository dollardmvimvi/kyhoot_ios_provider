//
//  MyEventsUpcomingVC.swift
//  LSP
//
//  Created by Rahul Sharma on 4/17/19.
//  Copyright © 2019 3Embed. All rights reserved.
//

import Foundation
import  JTCalendar


extension MyEventsViewController:JTCalendarDelegate {
    
    //setup weekly calendar
    func setupCalendar() {
        calendarManager = JTCalendarManager(locale: .current, andTimeZone: .current)!
        todayDate = Date.dateWithDifferentTimeInterval()
        calendarManager.delegate = self
        createMinAndMaxDate()
        
        calendarManager.menuView = calendarMenuView
        calendarManager.contentView = calendarView
        calendarManager.setDate(todayDate)
        calendarManager.settings?.weekModeEnabled = true
        calendarManager.reload()

    }
    
    //set min and maximum week
    func createMinAndMaxDate() {
        todayDate = Date.dateWithDifferentTimeInterval()
        // Min date will be 8 weeks before today
        minDate = calendarManager.dateHelper?.add(to: todayDate, weeks: -8)
        maxDate = calendarManager.dateHelper?.add(to: todayDate, weeks: 8)
        
    }
    
    func getBooking(byDate cal:JTCalendarManager, date: Date) {
        
        var currentCalendar =  cal.dateHelper?.calendar()
        currentCalendar?.timeZone = .current
        currentCalendar?.locale = .current
        
        //let startOfDay = currentCalendar?.startOfDay(for: cal.date())
        let startOfDay =  currentCalendar?.startOfDay(for: todayDate!)
        var components = DateComponents()
        components.day = 1
        components.second = -1
        let endOfDay = currentCalendar?.date(byAdding: components, to: startOfDay!)
        let startTimeStamp = startOfDay?.timeIntervalSince1970
        let endTimeStamp = endOfDay?.timeIntervalSince1970
       
        model.getAcceptedBookingByDate(from: startTimeStamp!, to: endTimeStamp!) { (responseData) in
            self.bookingData?.acceptedData = responseData.acceptedData
//            self.acceptData = responseData.acceptedData
            self.calendarManager.reload()
            self.upcomingTable.reloadData()
        }
    }
    
    @IBAction func preWeekBtnTapped(_ sender: Any) {
        DispatchQueue.main.async {
            self.calendarView.loadPreviousPageWithAnimation()
            self.view.layoutIfNeeded()
        }
        
    }
    
    @IBAction func nextWeekBtnTapped(_ sender: Any) {
        DispatchQueue.main.async {
            self.calendarView.loadNextPageWithAnimation()
            self.view.layoutIfNeeded()
        }
        
    }
    
    //calendar methods
    
    func calendar(_ calendar: JTCalendarManager?, prepareDayView dayView: (UIView & JTCalendarDay)?) {
        
        if   let dayview1 = dayView as? JTCalendarDayView{
            dayview1.textLabel?.font = UIFont(name: "BebasNeue-Book", size: dayview1.textLabel?.font.pointSize ?? 8.0)
            DispatchQueue.global().async{
                sleep(1);
                DispatchQueue.main.async{
                    if (self.calendarManager.dateHelper?.date(Date(), isTheSameDayThan: dayview1.date))! {
                        dayview1.circleView.isHidden = false
                        dayview1.circleView.backgroundColor = COLOR.APP_COLOR
                        dayview1.dotView.backgroundColor = UIColor.white
                        dayview1.textLabel?.textColor = UIColor.white
                    }
                    else if  (self.calendarManager.dateHelper?.date(self.todayDate, isTheSameDayThan: dayview1.date))! { //!dateSelected.isEmpty &&
                        dayview1.circleView.isHidden = false
                        dayview1.circleView.backgroundColor = Helper.UIColorFromRGB(rgbValue: 0x808080)
                        dayview1.dotView.backgroundColor = COLOR.APP_COLOR
                        dayview1.textLabel?.textColor = UIColor.white
                    }
                    else if !((self.calendarManager.dateHelper?.date(self.calendarView.date, isTheSameMonthThan: dayview1.date))!) {
                        dayview1.circleView.isHidden = true
                        dayview1.dotView.backgroundColor = COLOR.APP_COLOR
                        dayview1.textLabel?.textColor = UIColor.lightGray
                    }
                    else {
                        dayview1.circleView.isHidden = true
                        dayview1.dotView.backgroundColor = COLOR.APP_COLOR
                        dayview1.textLabel?.textColor = UIColor.black
                    }
                }
            }
        }
    }
    
    
    func calendar(_ calendar: JTCalendarManager?, didTouchDayView dayView: (UIView & JTCalendarDay)?) {
    
        if let dayview1 = dayView as? JTCalendarDayView{
            dayview1.textLabel?.font = UIFont(name: "BebasNeue-Book", size: dayview1.textLabel?.font.pointSize ?? 8.0)
            todayDate = dayview1.date
            dayview1.circleView.transform = CGAffineTransform.identity.scaledBy(x: 0.1, y: 0.1)
            UIView.transition(with: dayview1 as UIView , duration: 0.3, options: [], animations: {() -> Void in
                dayview1.circleView.transform = CGAffineTransform.identity
                self.calendarManager.reload()
                self.getBooking(byDate: calendar!, date: self.todayDate!)
                //self.scheduleTableview.reloadData()
            }) { _ in }
            if !(calendarManager.dateHelper!.date(calendarView.date, isTheSameMonthThan: dayview1.date)) {
                if calendarView.date.compare(dayview1.date) == .orderedAscending {
                    self.getBooking(byDate: calendar!, date: todayDate!)
                    calendarView.loadNextPageWithAnimation()
                    
                }
                else {
                    self.getBooking(byDate: calendar!, date: todayDate!)
                    calendarView.loadPreviousPageWithAnimation()
                }
            }
        }
    }
    
    // Used to limit the date for the calendar, optional
    
    func calendar(_ calendar: JTCalendarManager?, canDisplayPageWith date: Date?) -> Bool {
        return (calendarManager.dateHelper?.date(date, isEqualOrAfter: minDate, andEqualOrBefore: maxDate))!
    }
    
    func calendarDidLoadNextPage(_ calendar: JTCalendarManager?) {
        DispatchQueue.global().async{
            sleep(1);
            DispatchQueue.main.async{
                
                self.getBooking(byDate: calendar!, date: calendar!.date())
            }
        }
    }
    
    
    func calendarDidLoadPreviousPage(_ calendar: JTCalendarManager?) {
        
        DispatchQueue.global().async{
            sleep(1);
            DispatchQueue.main.async{
                
                self.getBooking(byDate: calendar!, date: calendar!.date())
                
            }
        }
    }
}
