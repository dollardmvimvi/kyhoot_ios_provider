//
//  WebViewVC.swift
//  LiveMPro
//
//  Created by Vengababu Maparthi on 26/10/17.
//  Copyright © 2017 3Embed. All rights reserved.
//

import UIKit
import WebKit

class WebViewVC: UIViewController,WKNavigationDelegate {
    
    @IBOutlet weak var titleLabel: UILabel!
    
    var webView: WKWebView!
    var urlFrom = ""
    var titleOfController = ""
    
    override func loadView() {
           webView = WKWebView()
           webView.navigationDelegate = self
           view = webView
       }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        titleLabel.text = titleOfController
        if urlFrom.length != 0 {
//            Helper.showPI(message: loading.load)
//            webView.loadRequest(NSURLRequest(url: URL(string: urlFrom)!) as URLRequest)
            let url = URL(string: urlFrom)
            let request = URLRequest(url: url!)
            self.webView.load(request)
            self.webView.allowsBackForwardNavigationGestures = true
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func backToVC(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)

    }
    
    
}
//extension WebViewVC : UIWebViewDelegate {
//    
//    func webViewDidStartLoad(_ webView: UIWebView) {
//        
//    }
//    
//    func webViewDidFinishLoad(_ webView: UIWebView) {
//        Helper.hidePI()
//    }
//    
//    func webView(_ webView: UIWebView, didFailLoadWithError error: Error) {
//        Helper.hidePI()
//    }
//}
