//
//  PBDocumentsCell.swift
//  DayRunner
//
//  Created by Rahul Sharma on 11/04/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import UIKit
import Kingfisher

protocol showTheVehicleDocPopupDelegate:class
{
    func showVehicleGalley(gallery: INSPhotosViewController)
}



class PBDocumentsCell: UITableViewCell {
    open weak var delegate: showTheVehicleDocPopupDelegate?
    var useCustomOverlay = false

    @IBOutlet var noJobPhotos: UILabel!
    @IBOutlet var documentSelected: UICollectionView!
        var photos: [INSPhotoViewable] = []
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func updatedTheJobImages(photo:String){
        photos = []
        let document = photo.components(separatedBy: ",")
        
        for dict in document {

            photos.append(INSPhoto(imageURL: URL(string: dict), thumbnailImageURL: URL(string:dict)))
        }
        documentSelected.reloadData()
    }
}

// MARK: - CollectionView datasource method
extension PBDocumentsCell:UICollectionViewDataSource{
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "vehicleDocs", for: indexPath as IndexPath) as! ShipmentCollectionViewCell
        
        cell.populateWithPhoto(photos[(indexPath as NSIndexPath).row])
        
        return cell
        
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if photos.count == 1 {
             noJobPhotos.isHidden = false
        }else{
             noJobPhotos.isHidden = true
        }
        return photos.count
    }
    
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
}

// MARK: - CollectionView delegate method
extension PBDocumentsCell:UICollectionViewDelegate{
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let cell = collectionView.cellForItem(at: indexPath) as! ShipmentCollectionViewCell
        let currentPhoto = photos[(indexPath as NSIndexPath).row]
        let galleryPreview = INSPhotosViewController(photos: photos, initialPhoto: currentPhoto, referenceView: cell)
        if useCustomOverlay {
            galleryPreview.overlayView = CustomOverlayView(frame: CGRect.zero)
        }
        
        galleryPreview.referenceViewForPhotoWhenDismissingHandler = { [weak self] photo in
            if let index = self?.photos.index(where: {$0 === photo}) {
                let indexPath = IndexPath(item: index, section: 0)
                return collectionView.cellForItem(at: indexPath) as? ShipmentCollectionViewCell
            }
            return nil
        }
        self.delegate?.showVehicleGalley(gallery: galleryPreview)
    }
}
