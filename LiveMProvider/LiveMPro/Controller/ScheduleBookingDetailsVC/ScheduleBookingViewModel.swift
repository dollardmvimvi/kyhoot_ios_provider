//
//  ScheduleBookingViewModel.swift
//  LiveMPro
//
//  Created by Vengababu Maparthi on 10/01/18.
//  Copyright © 2018 3Embed. All rights reserved.
//

import Foundation

import RxAlamofire
import RxCocoa
import RxSwift


class ScheduleBookingViewModel:NSObject {
    
    var apiCall = APILibrary()
    let disposebag = DisposeBag()
    
    

    /// get the booking details on schedule
    ///
    /// - Parameters:
    ///   - bookingID: gets the booking details using booking id
    ///   - completionHandler: returns the schedule model data on completion
    func getBookingSchedulebookingData(bookingID:String,completionHandler:@escaping (Bool,ScheduleBookModel) -> ()) {
        
        Helper.showPI(message:loading.load)
        
        let rxApiCall = HistoryAPI()
        rxApiCall.getBookingsHistoryAPi(method:API.METHOD.SCHEDULEBOOKINGDATA + "/" + bookingID)
        rxApiCall.History_Response
            .subscribe(onNext: {responseModel in
                
                let responseCodes : HTTPSResponseCodes = HTTPSResponseCodes(rawValue: responseModel.httpStatusCode)!
                Helper.hidePI()
                switch responseCodes{
                case .SuccessResponse:
                    if  let data =  responseModel.data["data"] as? [String:Any]{
                        completionHandler(true,ScheduleBookModel().parsingTheServiceResponse(acceptData: data))
                    }
                    break
                default:
                    Helper.alertVC(errMSG: responseModel.data["message"] as! String)
                    break
                }
            }, onError: {error in
                
                Helper.hidePI()
            }).disposed(by: disposebag)
    }
}
