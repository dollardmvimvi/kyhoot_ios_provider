//
//  AddCatDetailsTableCell.swift
//  LiveMPro
//
//  Created by Vengababu Maparthi on 13/11/17.
//  Copyright © 2017 3Embed. All rights reserved.
//

import UIKit
import Kingfisher

class AddCatDetailsTableCell: UITableViewCell {
    
    @IBOutlet weak var editExpriyDate: UIButton!
    @IBOutlet weak var certificateImageName: UILabel!
    
    @IBOutlet weak var expiryDateTF: FloatLabelTextField!
    @IBOutlet weak var otherTF: FloatLabelTextField!
    @IBOutlet weak var certificateImage: UIImageView!
    @IBOutlet weak var selectCertificate: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    func updateOtherFieldData(otherData:CatFieldModel?) {
        
        if otherData!.isMandatody == 1{
            otherTF.placeholder = otherData!.fieldName + " *"
        }else{
            otherTF.placeholder = otherData!.fieldName
        }
        if otherData!.othersTf.length > 0{
            otherTF.text =  otherData?.othersTf
        }else{
            if otherData!.data.length > 0{
                otherTF.text = otherData!.data
            }
        }
    }
    
    func expriyDateUpdate(expiryData:CatFieldModel?) {
        
        if expiryData!.isMandatody == 1{
            expiryDateTF.placeholder = expiryData!.fieldName + " *"
        }else{
            expiryDateTF.placeholder = expiryData!.fieldName
        }
        
        if expiryData!.expiryDate.length > 0{
            expiryDateTF.text = expiryData!.expiryDate
        }else{
            if expiryData!.data.length > 0{
                expiryDateTF.text = expiryData!.data
            }
        }
    }
    
    func updateDocImage(docDate:CatFieldModel? ,fromProfile:Int) {
        
        if docDate!.isMandatody == 1{
            certificateImageName.text = docDate!.fieldName + " *"
        }else{
            certificateImageName.text = docDate!.fieldName
        }
        if fromProfile == 0{
            if docDate!.docImage.image != nil{
                self.certificateImage.image = docDate!.docImage.image
            }
        }else{
            if docDate!.docImage.image != nil{
                self.certificateImage.image = docDate!.docImage.image
            }else{
                self.certificateImage.kf.setImage(with: URL(string: docDate!.data),
                                                  placeholder:UIImage.init(named: "upload_image_two"),
                                                  options: [.transition(ImageTransition.fade(1))],
                                                  progressBlock: { receivedSize, totalSize in
                },
                                                  completionHandler: { image, error, cacheType, imageURL in
                })
            }
        }
    }
}

