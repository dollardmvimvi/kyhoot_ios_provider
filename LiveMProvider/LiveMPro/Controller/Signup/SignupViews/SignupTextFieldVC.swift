//
//  SignupTextFieldVC.swift
//
//  Created by Vengababu Maparthi on 17/08/17.
//  Copyright © 2017 3Embed. All rights reserved.
//

import Foundation
import RxSwift
import RxKeyboard
import RxCocoa

// MARK: - Textfield delegate method
extension SignUpViewController : UITextFieldDelegate{
    
    func shake(textField:UITextField) {
        textField.shake(10, withDelta: 7, speed: 0.06, shakeDirection: .horizontal)
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        
        activeTextField = textField
        
        RxKeyboard.instance.visibleHeight
            .drive(onNext: { keyboardVisibleHeight in
                self.mainScrollView.contentInset.bottom = keyboardVisibleHeight + 20
            })
            .disposed(by: disposeBag)

        
        switch textField {
        case firstNAme:
            view1.backgroundColor = COLOR.APP_COLOR
            break
            
        case lastName:
            view2.backgroundColor = COLOR.APP_COLOR
            break
            
        case emailAddress:
            view3.backgroundColor = COLOR.APP_COLOR
            break
            
        case password:
            view5.backgroundColor = COLOR.APP_COLOR
            break
            
        case phoneNumber:
            view6.backgroundColor = COLOR.APP_COLOR
            break
            
        case referralcodeTF:
            view9.backgroundColor = COLOR.APP_COLOR
            break
            
        default:
            break
        }
        return true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {    //delegate method
        if textField == password && (emailAddress.text?.length)!>1{
            
            if !(emailAddress.text?.isValidEmail)!{
                shake(textField: emailAddress)
                emailAddress.text = ""
                emailAddress.becomeFirstResponder()
                self.present(Helper.alertVC(title: alertMsgCommom.Message, message:signup.InValidEmail), animated: true, completion: nil)
            }else{
                if !emailService {
                    self.validateEmail(typeValidate: "2", fromCity: false)
                }
            }
        }else if textField == phoneNumber && (password.text?.length)!>1{
            if Helper.isValidPassword(password:(password.text)!){
                phoneNumber.becomeFirstResponder()
            }else{
                shake(textField: password)
                password.becomeFirstResponder()
                password.text = ""
                self.present(Helper.alertVC(title: alertMsgCommom.Message, message:signup.PasswordInvalid), animated: true, completion: nil)
            }
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == phoneNumber {
            mobileService = false
            let maxLength = 11
            let currentString: NSString = textField.text! as NSString
            let newString: NSString =
                currentString.replacingCharacters(in: range, with: string) as NSString
            return newString.length <= maxLength
        }
        
        if textField == emailAddress {
            emailService = false
        }
        
        if textField ==  referralcodeTF{
            referralService = false
        }
        return true
    }
    
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {  //delegate method
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {  //delegate method
        
        switch textField {
        case firstNAme:
            if !(firstNAme.text?.isEmpty)!{
                view1.backgroundColor = COLOR.APP_COLOR
            }else{
                view1.backgroundColor = Helper.UIColorFromRGB(rgbValue: 0xe1e1e1)
            }
            break
            
        case lastName:
            if !(lastName.text?.isEmpty)!{
                view2.backgroundColor = COLOR.APP_COLOR
            }else{
                view2.backgroundColor = Helper.UIColorFromRGB(rgbValue: 0xe1e1e1)
            }
            break
        case emailAddress:
            if !(emailAddress.text?.isEmpty)!{
                view3.backgroundColor = COLOR.APP_COLOR
            }else{
                view3.backgroundColor = Helper.UIColorFromRGB(rgbValue: 0xe1e1e1)
            }
            break
            
        case password:
            if !(password.text?.isEmpty)!{
                view5.backgroundColor = COLOR.APP_COLOR
            }else{
                view5.backgroundColor = Helper.UIColorFromRGB(rgbValue: 0xe1e1e1)
            }
            break
            
        case phoneNumber:
            if !(phoneNumber.text?.isEmpty)!{
                view6.backgroundColor = COLOR.APP_COLOR
            }else{
                view6.backgroundColor = Helper.UIColorFromRGB(rgbValue: 0xe1e1e1)
            }
            break

        case referralcodeTF:
            if !(referralcodeTF.text?.isEmpty)!{
                view9.backgroundColor = COLOR.APP_COLOR
            }else{
                view9.backgroundColor = Helper.UIColorFromRGB(rgbValue: 0xe1e1e1)
            }

            if  !(referralcodeTF.text!.isEmpty) && !referralService{
                self.validateReferral()
                referralService = true
            }
            break
        default:
            
            break
        }
        
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {   //delegate method
        
        if textField == firstNAme{
            self.lastName.becomeFirstResponder()
            return false
            
        }else if textField == lastName{
            self.emailAddress.becomeFirstResponder()
            return false
            
        }else if textField == emailAddress{
            if selectYourCity.text!.length > 1{
                self.password.becomeFirstResponder()
                return false
            }else{
                self.selectCity(AnyObject.self)
                dismisskeyBord()
            }
            
        }else if textField == password{
            self.phoneNumber.becomeFirstResponder()
            return false
            
        }else if textField == phoneNumber{
            dismisskeyBord()
            
        }else{
            dismisskeyBord()
        }
        return true
    }
}


