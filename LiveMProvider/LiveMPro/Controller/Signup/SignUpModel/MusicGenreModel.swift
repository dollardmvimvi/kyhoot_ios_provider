//
//  VehicleMakeModel.swift
//  DayRunnerDriver
//
//  Created by Rahul Sharma on 08/05/17.
//  Copyright © 2017 3Embed. All rights reserved.
//


import UIKit

class Categories {
    
    var catID = ""
    var catName = ""
    var categorySelected = 0
    
    var categoryDocs = [CatModel]()
    var subCategories = [SubCatModel]()
    
    //City and category under city data response handling
    
    class func parsingTheServiceResponse(responseData:[[String: Any]]) -> [Categories]{
        var categories = [Categories]()
        for items in responseData{
            var subCat = [CatModel]()
            let category = Categories()
            if  let id = items["catId"] as? String{
                category.catID = id
            }
            
            if let name = items["catName"] as? String{
                category.catName = name
            }
            
            for categorySub in (items["document"] as? [[String:Any]])!{
                let subCatModel = CatModel()
                var fieldModelData = [CatFieldModel]()
                if let documentName = categorySub["docName"] as? String{
                    subCatModel.docName = documentName
                }
                
                if let docId = categorySub["_id"] as? String{
                    subCatModel.docID = docId
                }
                
                if let name = categorySub["isManadatory"] as? Int{
                    subCatModel.isMandatody = name
                }
                
                for fieldDict in (categorySub["field"] as? [[String:Any]])!{
                    let fieldModel = CatFieldModel()
                    
                    if let fieldName = fieldDict["fName"] as? String{
                        fieldModel.fieldName = fieldName
                    }
                    
                    if let fieldId = fieldDict["fId"] as? String{
                        fieldModel.fieldID = fieldId
                    }
                    
                    if let fieldType =  fieldDict["fType"] as? Int{
                        fieldModel.type = fieldType
                    }
                    
                    if let fieldMandatory =  fieldDict["isManadatory"] as? Int{
                        fieldModel.isMandatody = fieldMandatory
                    }
                    
                    if let data = fieldDict["data"] as? String{
                        fieldModel.data = data
                    }
                    
                    fieldModel.docImage.image = nil
                    fieldModelData.append(fieldModel)
                }
                subCatModel.subCatArray = fieldModelData
                subCat.append(subCatModel)
            }
            
            for subCatData in (items["subCategory"] as? [[String:Any]])!{
                let subcatModel = SubCatModel()
                
                if let subID = subCatData["_id"] as? String{
                    subcatModel.subCatID = subID
                }
                
                if let subName = subCatData["sub_cat_name"] as? String{
                    subcatModel.subCatName = subName
                }
                
                category.subCategories.append(subcatModel)
            }
            
            category.categoryDocs = subCat
            categories.append(category)
        }
        return categories
    }
    
    class func parsingTheCategoryServiceResponse(responseData:[[String: Any]]) -> [Categories]{
        var categories = [Categories]()
        for items in responseData{
            var subCat = [CatModel]()
            let category = Categories()
            if  let id = items["categoryId"] as? String{
                category.catID = id
            }
            
            if let name = items["categoryName"] as? String{
                category.catName = name
            }
            
            for categorySub in (items["documents"] as? [[String:Any]])!{
                let subCatModel = CatModel()
                var fieldModelData = [CatFieldModel]()
                if let documentName = categorySub["docName"] as? String{
                    subCatModel.docName = documentName
                }
                
                
                for fieldDict in (categorySub["field"] as? [[String:Any]])!{
                    let fieldModel = CatFieldModel()
                    
                    if let fieldName = fieldDict["fName"] as? String{
                        fieldModel.fieldName = fieldName
                    }
                    
                    if let fieldId = fieldDict["fId"] as? String{
                        fieldModel.fieldID = fieldId
                    }
                    
                    if let fieldType =  fieldDict["fType"] as? Int{
                        fieldModel.type = fieldType
                    }
                    
                    if let fieldMandatory =  fieldDict["isManadatory"] as? Int{
                        fieldModel.isMandatody = fieldMandatory
                    }
                    
                    if let data = fieldDict["data"] as? String{
                        fieldModel.data = data
                    }
                    
                    fieldModel.docImage.image = nil
                    fieldModelData.append(fieldModel)
                }
                subCatModel.subCatArray = fieldModelData
                subCat.append(subCatModel)
            }
            category.categoryDocs = subCat
            categories.append(category)
        }
        return categories
    }
}

