//
//  WalletCurrentCreditCell.swift
//  DayRunner
//
//  Created by apple on 9/6/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import UIKit

class WalletCurrentCreditCell: UITableViewCell {

    @IBOutlet weak var recentTrasactionBtn: UIButton!
    
    @IBOutlet weak var hardLimitLbl: UILabel!
    
    @IBOutlet weak var softLimitLbl: UILabel!
    
    @IBOutlet weak var currentCreditLbl: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
}
