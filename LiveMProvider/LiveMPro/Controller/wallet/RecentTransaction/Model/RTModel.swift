//
//  RTModel.swift
//  DayRunner
//
//  Created by apple on 9/11/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import Foundation

import RxAlamofire
import RxCocoa
import RxSwift

class RTModel: NSObject {
    
    var apiCall = APILibrary()
    let disposebag = DisposeBag()
    
    
    ///Get Recent Transactions Data
    ///
    /// - Parameters:
    /// - index: Index number
    /// - success: return an array of RTModel
     func recentTractionsData(index: Int,
                                   success:@escaping (RTDataType?) -> ()) {
        
        Helper.showPI(message:loading.load)
        let rxApiCall = WalletAPI()
       rxApiCall.getTheRecentTransaction(index:index)
        rxApiCall.Wallet_Response
            .subscribe(onNext: {responseModel in
                
                let responseCodes : HTTPSResponseCodes = HTTPSResponseCodes(rawValue: responseModel.httpStatusCode)!
                Helper.hidePI()
                switch responseCodes{
                case .SuccessResponse:
                    if let data = responseModel.data["data"] as? [String:Any] {
                        success(RTDataType(data: data))
                    }
                    break
                default:
                    Helper.alertVC(errMSG: responseModel.data["message"] as! String)
                    break
                }
            }, onError: {error in
                
                Helper.hidePI()
            }).disposed(by: disposebag)
    }
}


