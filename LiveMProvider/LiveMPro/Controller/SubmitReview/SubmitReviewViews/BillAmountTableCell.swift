//
//  BillAmountTableCell.swift
//  LiveMPro
//
//  Created by Vengababu Maparthi on 04/10/17.
//  Copyright © 2017 3Embed. All rights reserved.
//

import UIKit

class BillAmountTableCell: UITableViewCell {
    @IBOutlet weak var billAmount: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
