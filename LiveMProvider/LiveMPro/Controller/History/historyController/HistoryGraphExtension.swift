//
//  HistoryGraphExtension.swift
//  LiveMPro
//
//  Created by Vengababu Maparthi on 07/12/17.
//  Copyright © 2017 3Embed. All rights reserved.
//

import Foundation

extension HistoryViewController{
    
    // this function use it to set up the chart
    func setChart(dataPoints:[String], value:[Double] , maxValue:Double)  {
        graphInitiated = true
        values = value
        textLabel =   months
        barColors = [COLOR.APP_COLOR, UIColor.red, UIColor.black, UIColor.orange, UIColor.purple, UIColor.green]
        currentBarColor = 0
        
        let width = UIScreen.main.bounds.size.width - 40
        
        var chartFrame = CGRect()
        if iPHONE.IS_iPHONE_5{
            chartFrame = CGRect(x: 0,
                                y: 10,
                                width: width,//barGraph.frame.size.width - 60,
                                height: barGraph.frame.size.height - 20)
            simpleBarGraph = SimpleBarChart(frame: chartFrame)
            simpleBarGraph.barWidth = 30.0
        }else if iPHONE.IS_iPHONE_6{
            chartFrame = CGRect(x: 0,
                                y: 10,
                                width: width,//barGraph.frame.size.width - 10,
                                height: barGraph.frame.size.height  - 20)
            simpleBarGraph = SimpleBarChart(frame: chartFrame)
            simpleBarGraph.barWidth = 35.0
        }else if iPHONE.IS_iPHONE_6_Plus{
            chartFrame = CGRect(x: 0,
                                y: 10,
                                width: width,//barGraph.frame.size.width + 10,
                                height: barGraph.frame.size.height  - 20)
            simpleBarGraph = SimpleBarChart(frame: chartFrame)
            simpleBarGraph.barWidth = 38.0
        }else{
            chartFrame = CGRect(x: 0,
                                y: 10,
                                width: width,//barGraph.frame.size.width - 10,
                                height: barGraph.frame.size.height  - 20)
            simpleBarGraph = SimpleBarChart(frame: chartFrame)
            simpleBarGraph.barWidth = 36.0
        }
        
        simpleBarGraph.delegate = self
        simpleBarGraph.dataSource = self
        simpleBarGraph.barShadowOffset = CGSize(width: 2.0, height: 1.0)
        simpleBarGraph.barShadowColor = COLOR.F8F8F8
        simpleBarGraph.barShadowAlpha = 0.5
        simpleBarGraph.barShadowRadius = 1.0
        simpleBarGraph.animationDuration = 0
        simpleBarGraph.xLabelType = SimpleBarChartXLabelTypeHorizontal
        
        if maxValue == 1{
            simpleBarGraph.incrementValue = CGFloat(0.5)
        }else if maxValue == 2 || maxValue == 3 || maxValue == 4{
            simpleBarGraph.incrementValue = CGFloat(1)
        }else{
            simpleBarGraph.incrementValue = CGFloat(maxValue/4)
        }
        
        simpleBarGraph.barTextType = SimpleBarChartBarTextTypeTop
        simpleBarGraph.barTextColor = UIColor.clear
        barGraph.addSubview(simpleBarGraph)
        simpleBarGraph.reloadData()
    }
}

extension HistoryViewController : SimpleBarChartDelegate{
    
}

extension HistoryViewController : SimpleBarChartDataSource{
    func numberOfBars(in barChart: SimpleBarChart!) -> UInt {
        return UInt(values.count);
    }
    
    func barChart(_ barChart: SimpleBarChart!, valueForBarAt index: UInt) -> CGFloat {
        let myFloat = NSNumber.init(value: values[Int(index)]).intValue
        let myCGFloat = CGFloat(myFloat)
        return myCGFloat
    }
    
    func barChart(_ barChart: SimpleBarChart!, textForBarAt index: UInt) -> String! {
        let doubleStr = String(format: "%.2f", values[Int(index)])
        return doubleStr
    }
    
    func barChart(_ barChart: SimpleBarChart!, colorForBarAt index: UInt) -> UIColor! {
        return barColors[currentBarColor] as! UIColor
    }
    
    func barChart(_ barChart: SimpleBarChart!, xLabelForBarAt index: UInt) -> String! {
        return textLabel[Int(index)] as! String
    }
    
}


