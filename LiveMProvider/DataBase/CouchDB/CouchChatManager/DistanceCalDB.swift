//
//  DistanceCalDB.swift
//  LiveMPro
//
//  Created by Vengababu Maparthi on 18/01/18.
//  Copyright © 2018 3Embed. All rights reserved.
//

import Foundation

class DistanceCalculation: NSObject {
    
    var chatDB = Couchbase.sharedInstance
    
    func getTheSavedDistance(distanceDocID: String)->[String:Any]{
        if let doc = chatDB.database.existingDocument(withID:distanceDocID){
            let properties = doc.properties
            if let dict = properties!["distanceDict"] as? [String:Any]{
                return dict
            }else{
                return [:]
            }
        }else{
            return [:]
        }
    }
    
    func addTheNewDistance(distanceDocID: String, dict:[String:Any]) {
        if let doc = chatDB.database.existingDocument(withID:distanceDocID){
            var properties = doc.properties
            properties!["distanceDict"] = dict["distanceDict"]
            try! doc.putProperties(properties!)
        }else{
            let distanceData:[String:Any] = ["distanceDict":dict["distanceDict"] as Any]
            chatDB.createDocumentWithID(sendDocumentID: distanceDocID, data: distanceData)
        }
    }
}
