//
//  LocationDBManager.swift
//  LiveMPro
//
//  Created by Vengababu Maparthi on 25/12/17.
//  Copyright © 2017 3Embed. All rights reserved.
//

import Foundation



class LocationDBManager:NSObject {
    var chatDB = Couchbase.sharedInstance
    
    @objc func getTheLocationDict(forUserID: String)->[String]{
        if let doc = chatDB.database.existingDocument(withID:forUserID){
            let properties = doc.properties
            if let dict = properties!["latLongs"] as? [String]{
                return dict
            }else{
                return []
            }
        }else{
            return []
        }
    }
    
    @objc func saveThelocationData(forUserID: String, dict:[String:Any]) {
        if let doc = chatDB.database.existingDocument(withID:forUserID){
            var properties = doc.properties
            properties!["latLongs"] = dict["latLongs"]
            try! doc.putProperties(properties!)
        }else{
            let finalDict:[String:Any] = ["latLongs":dict["latLongs"] as Any]
            let documentID = chatDB.createDocument(withProperties: finalDict)
            UserDefaults.standard.set(documentID, forKey: "documentID")
            UserDefaults.standard.synchronize()
        }
    }
}
