//
//  RateCardDBManager.swift
//  LSP
//
//  Created by Vengababu Maparthi on 14/02/18.
//  Copyright © 2018 3Embed. All rights reserved.
//

import Foundation


class RateCardDBManager:NSObject {
    var rateCardDB = Couchbase.sharedInstance
    
    func getAllRateCardData(forUserID: String, completion: @escaping ([[String:Any]]) -> ()) {
        if let doc = rateCardDB.database.existingDocument(withID:forUserID){
            let properties = doc.properties
            if let dict = properties!["rateCardDict"] as? [[String:Any]]{
                completion(dict)
            }
        }
    }
    
    func saveTheRateCardData(forUserID: String, dict:[String:Any]) {
        if let doc = rateCardDB.database.existingDocument(withID:forUserID){
            var properties = doc.properties
            properties!["rateCardDict"]! = dict["rateCardDict"] as Any
            try! doc.putProperties(properties!)
        }else{
            rateCardDB.createDocumentWithID(sendDocumentID: forUserID, data: dict)
        }
    }
}

