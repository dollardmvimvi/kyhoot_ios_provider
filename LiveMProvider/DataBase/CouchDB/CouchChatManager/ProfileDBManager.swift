//
//  ProfileDBManager.swift
//  LiveMPro
//
//  Created by Vengababu Maparthi on 20/12/17.
//  Copyright © 2017 3Embed. All rights reserved.
//

import Foundation


class ProfileDBManager:NSObject {
    var profileDB = Couchbase.sharedInstance
    
    func getAllProfileData(forUserID: String, completion: @escaping ([String:Any]) -> ()) {
        if let doc = profileDB.database.existingDocument(withID:forUserID){
            let properties = doc.properties
            if let dict = properties!["profileDict"] as? [String:Any]{
                completion(dict)
            }
        }
    }
    
    func saveTheMessage(forUserID: String, dict:[String:Any]) {
        if let doc = profileDB.database.existingDocument(withID:forUserID){
            var properties = doc.properties
            properties!["profileDict"]! = dict["profileDict"] as Any
            try! doc.putProperties(properties!)
        }else{
            profileDB.createDocumentWithID(sendDocumentID: forUserID, data: dict)
        }
    }
}

