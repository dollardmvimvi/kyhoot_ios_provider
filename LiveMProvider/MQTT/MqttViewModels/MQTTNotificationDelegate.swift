//
//  MQTTNotificationDelegate.swift
//  MQTT Chat Module
//
//  Created by Rahul Sharma on 29/07/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import Foundation
import UserNotifications
import CocoaLumberjack

class MQTTNotificationDelegate: NSObject, UNUserNotificationCenterDelegate {
    
    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                willPresent notification: UNNotification,
                                withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        // Play sound and show alert to the user
        completionHandler([.alert,.sound])
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                didReceive response: UNNotificationResponse,
                                withCompletionHandler completionHandler: @escaping () -> Void) {
        
        // Determine the user action
        switch response.actionIdentifier {
        case UNNotificationDismissActionIdentifier:
            DDLogDebug("Dismiss Action")
        case UNNotificationDefaultActionIdentifier:
            DDLogDebug("Default")
        case "Snooze":
            DDLogDebug("Snooze")
        case "Delete":
            DDLogDebug("Delete")
        default:
            DDLogDebug("Unknown action")
        }
        completionHandler()
    }
}

