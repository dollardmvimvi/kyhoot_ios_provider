//
//  DGElasticPullHelper.swift
//  Trustpals
//
//  Created by Vasant Hugar on 28/02/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import UIKit

extension UITableView {
    
    func addPullToRefresh(completion: @escaping(_ refreshed: Bool) -> Void) {
        
        // pull to refresh
        let loadingView = DGElasticPullToRefreshLoadingViewCircle()
        loadingView.tintColor = UIColor.white
        self.dg_addPullToRefreshWithActionHandler({ [weak self] () -> Void in
            DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + Double(Int64(1.5 * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC), execute: {
                
                completion(true)
                self?.dg_stopLoading()
                
            })
            }, loadingView: loadingView)
        self.dg_setPullToRefreshFillColor(COLOR.APP_COLOR)
        self.dg_setPullToRefreshBackgroundColor(self.backgroundColor!)
    }
}
