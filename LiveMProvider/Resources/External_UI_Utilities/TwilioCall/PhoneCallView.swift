//
//  PhoneCallView.swift
//  Twilio-Call-Swift
//
//  Created by Rahul Sharma on 15/12/17.
//  Copyright © 2017 3Embed. All rights reserved.
//

import UIKit
import AVFoundation

class PhoneCallView:UIView {
    
    
    @IBOutlet var speakerButton: UIButton!
    
    @IBOutlet var providerImageView: UIImageView!
    
    @IBOutlet var timeLabel: UILabel!
    
    @IBOutlet var providerNameLabel: UILabel!
    
    @IBOutlet var endCallButton: UIButton!
    
    static let windows = UIApplication.shared.keyWindow
    
    
    var callTimer:Timer!
    var hours = 0
    var minutes = 0
    var seconds = 0
    
    
    //MARK: - Initial Methods -
    
    private static var share: PhoneCallView? = nil
    
    static var sharedInstance: PhoneCallView {
        
        if share == nil {
            
            share = Bundle(for: self).loadNibNamed("PhoneCallView",
                                                   owner: nil,
                                                   options: nil)?.first as? PhoneCallView
            
            share?.frame = UIScreen.main.bounds
        
        }
        
        return share!
    }

    
    @objc func showTimer() {
        
        seconds += 1
        
        if seconds == 60 {
            minutes += 1
            seconds = 0
        }
        
        if minutes == 60 {
            hours += 1
            minutes = 0
        }
        
        showJobTimerValues()
    }
    
    /**
     *  show Job Timer Values On Screen Every Second
     */
    
    func showJobTimerValues() {
        
        var hoursValue: String
        var minutesValue: String
        var secondsValue: String
        
        if seconds < 10 {
            secondsValue = "0\(seconds)"
        }
        else {
            secondsValue = "\(seconds)"
        }
        
        if minutes < 10 {
            minutesValue = "0\(minutes)"
        }
        else {
            minutesValue = "\(minutes)"
        }
        
        if hours < 10 {
            hoursValue = "0\(hours)"
        }
        else {
            hoursValue = "\(hours)"
        }
        timeLabel.text = "\(hoursValue):\(minutesValue):\(secondsValue)"
        print("Job Timer = \(timeLabel.text!)")
        
    }

    
    
    func startTimer() {
        
        if callTimer != nil {
            
            callTimer.invalidate()
            callTimer = nil
        }
        
        callTimer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(self.showTimer), userInfo: nil, repeats: true)

    }
    
    
    func endTimer() {
        
        if callTimer != nil {
            
            callTimer.invalidate()
            callTimer = nil
        }

        removeFromSuperview()
        PhoneCallView.share = nil

    }
    
    
    @IBAction func speakerButtonAction(_ sender: Any) {
        
        let speakerBtn = sender as? UIButton
        if speakerBtn?.isSelected ?? false {
            
            makeSpeakerOFF()
            speakerBtn?.isSelected = false
        }
        else {
            
            makeSpeakerON()
            speakerBtn?.isSelected = true
        }
        
    }
    
    
    @IBAction func endCallButtonAction(_ sender: Any) {
        
        OnBookingViewController.sharedInstance().hangUpCall()
        
    }

    
    
    func makeSpeakerON() {
        
        let session = AVAudioSession.sharedInstance()
        try? session.overrideOutputAudioPort(AVAudioSessionPortOverride.speaker)
    }
    
    func makeSpeakerOFF() {
        
        let session = AVAudioSession.sharedInstance()
        try? session.overrideOutputAudioPort(AVAudioSessionPortOverride.none)
    }
    
}
