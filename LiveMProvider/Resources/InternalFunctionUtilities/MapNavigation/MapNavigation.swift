//
//  MapNavigation.swift
//  DayRunnerDriverDev
//
//  Created by Vengababu Maparthi on 12/08/17.
//  Copyright © 2017 3Embed. All rights reserved.
//

import Foundation
class MapNavigation: NSObject {
    
    
    
    class func navgigateTogoogleMaps(latit:Double,logit:Double){
        
        if (UIApplication.shared.canOpenURL(NSURL(string:"comgooglemaps://")! as URL)) {
            
            UIApplication.shared.openURL(URL(string:
                "comgooglemaps://?saddr=&daddr=\(String(describing: latit)),\(String(describing: logit))&directionsmode=driving")!)
            
        } else {
            let link = "https://itunes.apple.com/us/app/id585027354?mt=8"
            UIApplication.shared.openURL(NSURL(string: link)! as URL)
            UIApplication.shared.isIdleTimerDisabled = true
        }
    }
    
    
    class func navgigateToWazeMaps(latit:Double,logit:Double){
        var link:String = "waze://"
        let url:NSURL = NSURL(string: link)!
        if UIApplication.shared.canOpenURL(url as URL) {
            let urlStr:NSString = NSString(format: "waze://?ll=%f,%f&navigate=yes",latit, logit)
            UIApplication.shared.openURL(NSURL(string: urlStr as String)! as URL)
            UIApplication.shared.isIdleTimerDisabled = true
        } else {
            link = "http://itunes.apple.com/us/app/id323229106"
            UIApplication.shared.openURL(NSURL(string: link)! as URL)
            UIApplication.shared.isIdleTimerDisabled = true
        }
    }
    
}
