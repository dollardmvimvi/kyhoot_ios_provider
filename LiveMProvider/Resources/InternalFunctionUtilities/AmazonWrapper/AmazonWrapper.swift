//
//  File.swift
//  AmazonWrapper
//
//  Created by Apple on 27/03/17.
//  Copyright © 2017 Apple. All rights reserved.
//

import UIKit
import AWSS3
import RxAlamofire
import Alamofire
import RxSwift
import RxCocoa

let AmazonAccessKey  = "AKIAJK5GZD3MBEXHE4DA"
let AmazonSecretKey  = "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"
let Bucket  = "kyhoot2"
//let Bucket  = "appscrip/localGenie"
let AWSPoolId = "ap-southeast-1:f59709f0-6744-4735-a54a-5a74acbea989"
let AmazonItemUrl = "https://s3.amazonaws.com/"



protocol AmazonWrapperDelegate {
    /**
     *  Facebook login is success
     *
     *  @param userInfo Userdict
     */
    
    func didImageUploadedSuccessfully(withDetails imageURL: String)
    /**
     *  Login failed with error
     *
     *  @param error error
     */
    
    func didImageFailtoUpload(_ error: Error?)
    

}

class AmazonWrapper: NSObject {
    
    static var share:AmazonWrapper?
    var delegate: AmazonWrapperDelegate?

    class func sharedInstance() -> AmazonWrapper {
        
        if (share == nil) {
            
            share = AmazonWrapper.self()
          
        }
        return share!
    }
    
    override init() {
        super.init()
        
    }
    
    func setConfigurationWithRegion(_ regionType: AWSRegionType, accessKey: String, secretKey: String) {
        
        let credentialsProvider = AWSStaticCredentialsProvider(accessKey: accessKey, secretKey: secretKey)
        let configuration = AWSServiceConfiguration(region: regionType, credentialsProvider: credentialsProvider)
        AWSServiceManager.default().defaultServiceConfiguration = configuration
    }
    
    
    /// Method to Create AWWS3 service configuration using PoolId
    ///
    /// - Parameter regionType: bucket region
    func setPoolConfigurationWithRegion(_ regionType: AWSRegionType) {
        
        let poolCredentialProvider = AWSCognitoCredentialsProvider.init(regionType: regionType, identityPoolId: AWSPoolId)
        let configuration = AWSServiceConfiguration.init(region: regionType, credentialsProvider:poolCredentialProvider)
        AWSServiceManager.default().defaultServiceConfiguration = configuration
    }

    func uploadImageToAmazon(withImage image: UIImage, imgPath:String,arg: Bool, completion: @escaping (Bool,String) -> ()) {
        print("First line of code executed")
       
        let formatter: DateFormatter = DateFormatter.initTimeZoneDateFormat()
        formatter.dateFormat = "yyyyMMddhhmmssa"
        
        var paths: [AnyObject] = NSSearchPathForDirectoriesInDomains(.cachesDirectory, .userDomainMask, true) as [AnyObject]
        let documentsDirectory: String = paths[0] as! String
        
        let photoURLPath = NSURL(fileURLWithPath: documentsDirectory)
        let getImagePath  = photoURLPath.appendingPathComponent("\(formatter.string(from:Date())).png")
       
        if !FileManager.default.fileExists(atPath: getImagePath!.path) {
            do {
                try UIImageJPEGRepresentation(image, 1.0)?.write(to: getImagePath!)
                print("file saved")
            }catch {
                print("error saving file")
            }
        }
        else {
            print("file already exists")
        }
        let uploadingImgData = image.jpeg(.medium)
        requestWith1(imageData: uploadingImgData , onCompletion: { (status, resUrl) in
            if status {
                completion(true,resUrl)
            }
        }) { (err) in
            completion(false,"")
        }
  
//        AWSS3TransferUtility.default().uploadFile(getImagePath!,
//                                                  bucket: Bucket,
//                                                  key: imgPath,
//                                                  contentType: "image/png", expression:nil) { (task, error) in
//
//                                                    if (error != nil) {
//
//                                                        if (self.delegate != nil)  {
//                                                            self.delegate?.didImageFailtoUpload(error)
//                                                        }
//
//                                                    }
//                                                    else {
//                                                        let uploadedImageURL = String(format:"https://s3.eu-west-2.amazonaws.com/%@/%@",Bucket,imgPath)
//
//                                                         completion(arg,uploadedImageURL)
//                                                        if (self.delegate != nil)  {
//                                                            self.delegate?.didImageUploadedSuccessfully(withDetails: uploadedImageURL)
//                                                        }
//
//                                                    }
//
//        }
        
    }
    
    func requestWith1(imageData: Data?, onCompletion: ((Bool,String) -> Void)? = nil, onError: ((Error?) -> Void)? = nil){
        
        let url = API.BASE_URL + "uploadImageOnServer"
        
        Alamofire.upload(multipartFormData: { multipartFormData in
            
            //            for (key, value) in parameters {
            //                if let data = (value as AnyObject).data(using: String.Encoding.utf8.rawValue) {
            //                    multipartFormData.append(data, withName: key)
            //                }
            //            }
            multipartFormData.append(imageData!, withName: "photo", fileName: "image.jpg", mimeType: "image/jpeg")
        },
                         to: url,method:HTTPMethod.post,
                         headers:RXNetworkHelper.getAOTHHeader(), encodingCompletion: { encodingResult in
                            switch encodingResult {
                            case .success(let upload, _, _):
                                upload
                                    .validate()
                                    .responseJSON { response in
                                        switch response.result {
                                        case .success(let value):
                                            do {
                                                if let dictonary = try JSONSerialization.jsonObject(with: response.data!, options: []) as? [String:Any]{
                                                    onCompletion!(true,dictonary["data"] as! String)
                                                }
                                            } catch let error as NSError {
                                                print(error)
                                            }
                                            
                                            print("responseObject: \(value)")
                                        case .failure(let responseError):
                                            print("responseError: \(responseError.localizedDescription)")
                                            onCompletion!(false,"")
                                        }
                                }
                            case .failure(let encodingError):
                                onCompletion!(false,"")
                                Helper.hidePI()
                                print("encodingError: \(encodingError.localizedDescription)")
                            }
        })
    }

    
    
   }

extension UIImage {
    enum JPEGQuality: CGFloat {
        case lowest  = 0
        case low     = 0.25
        case medium  = 0.5
        case high    = 0.75
        case highest = 1
    }
    
    /// Returns the data for the specified image in JPEG format.
    /// If the image object’s underlying image data has been purged, calling this function forces that data to be reloaded into memory.
    /// - returns: A data object containing the JPEG data, or nil if there was a problem generating the data. This function may return nil if the image has no data or if the underlying CGImageRef contains data in an unsupported bitmap format.
    func jpeg(_ quality: JPEGQuality) -> Data? {
        return UIImageJPEGRepresentation(self, quality.rawValue)
    }
}
